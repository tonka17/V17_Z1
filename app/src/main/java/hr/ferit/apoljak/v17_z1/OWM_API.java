package hr.ferit.apoljak.v17_z1;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Student on 15.9.2017..
 */

public interface OWM_API {

    static String BASE_URL = "http://api.openweathermap.org/";
    static String API_KEY = "e7871c832e3bfdab74b97143475066cb";
    String UNIT = "metric";
    String IMAGE_URL = "http://openweathermap.org/img/w/";


    @GET("data/2.5/forecast")
    Call<SearchResult> getCity(@Query("lat") String coorLat,
                               @Query("lon") String coorLon,
                               @Query("units") String unit,
                               @Query("APPID") String apiKey);

}
