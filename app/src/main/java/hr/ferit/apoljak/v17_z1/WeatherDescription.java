package hr.ferit.apoljak.v17_z1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Student on 15.9.2017..
 */

class WeatherDescription {
    @SerializedName("id") private int _Id;
    @SerializedName("main") private String _Main;
    @SerializedName("description") private String _Description;
    @SerializedName("icon") private String _Icon;

    public WeatherDescription() {
    }

    public WeatherDescription(int _Id, String _Main, String _Description, String _Icon) {
        this._Id = _Id;
        this._Main = _Main;
        this._Description = _Description;
        this._Icon = _Icon;
    }

    public int get_Id() {
        return _Id;
    }

    public String get_Main() {
        return _Main;
    }

    public String get_Description() {
        return _Description;
    }

    public String get_Icon() {
        return _Icon;
    }
}
