package hr.ferit.apoljak.v17_z1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonija on 05/10/2017.
 */

class Snow {
    @SerializedName("3h") private double _3hSnow;

    public Snow() {
    }

    public Snow(double _3hSnow) {
        this._3hSnow = _3hSnow;
    }

    public double get3hSnow() {
        return _3hSnow;
    }

}
