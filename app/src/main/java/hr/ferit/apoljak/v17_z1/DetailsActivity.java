package hr.ferit.apoljak.v17_z1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends Activity {

    public static final String KEY_DATE = "date";
    public static final String KEY_HUMIDITY = "humidity";
    public static final String KEY_PRESSURE = "pressure";
    public static final String KEY_WINDSPEED = "windspeed";

    @BindView(R.id.tvDetailsDate) TextView tvDetailsDate;
    @BindView(R.id.tvHumidity) TextView tvHumidity;
    @BindView(R.id.tvPressure) TextView tvPressure;
    @BindView(R.id.tvWindSpeed) TextView tvWindSpeed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        this.startHandlingIntent(this.getIntent());
    }

    private void startHandlingIntent(Intent intent) {
        if(intent!=null)
        {
            if(intent.hasExtra(KEY_DATE))
            {
                tvDetailsDate.setText(intent.getStringExtra(KEY_DATE));
            }
            if(intent.hasExtra(KEY_HUMIDITY))
            {
                tvHumidity.setText(intent.getStringExtra(KEY_HUMIDITY));
            }
            if (intent.hasExtra(KEY_PRESSURE))
            {
                tvPressure.setText(intent.getStringExtra(KEY_PRESSURE));
            }
            if (intent.hasExtra(KEY_WINDSPEED))
            {
                tvWindSpeed.setText(intent.getStringExtra(KEY_WINDSPEED));
            }
        }
    }

}
