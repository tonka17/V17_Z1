package hr.ferit.apoljak.v17_z1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends Activity implements Callback<SearchResult> {

    @BindView(R.id.etLatQuery)
    EditText etLatQuery;
    @BindView(R.id.etLonQuery)
    EditText etLonQuery;
    @BindView(R.id.lvWeather) ListView lvWeather;
    @BindView(R.id.bSearchCity)
    Button bSearchCity;
    @BindView(R.id.tvCityName)
    TextView tvCityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bSearchCity)
    public void ShowTemperature()
    {
        String lat = String.valueOf(etLatQuery.getText());
        String lon = String.valueOf(etLonQuery.getText());


        Retrofit retrofit = new Retrofit.Builder().baseUrl(OWM_API.BASE_URL).client(new OkHttpClient()).addConverterFactory(GsonConverterFactory.create()).build();
        OWM_API api = retrofit.create(OWM_API.class);

        Call<SearchResult> request = api.getCity(lat, lon, OWM_API.UNIT, OWM_API.API_KEY);
        request.enqueue(this);
    }

    @OnItemClick(R.id.lvWeather)
    public void showDetails(int position)
    {
        Forecast forecast = (Forecast) this.lvWeather.getAdapter().getItem(position);
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.KEY_DATE, forecast.get_DtText());
        intent.putExtra(DetailsActivity.KEY_HUMIDITY, String.valueOf(forecast.get_main().get_Humidity()));
        intent.putExtra(DetailsActivity.KEY_PRESSURE, String.valueOf(forecast.get_main().get_Pressure()));
        intent.putExtra(DetailsActivity.KEY_WINDSPEED, String.valueOf(forecast.get_Wind().get_speed()));
        this.startActivity(intent);
    }

    @Override
    public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
        SearchResult result = response.body();
        WeatherAdapter adapter = new WeatherAdapter(result);
        this.lvWeather.setAdapter(adapter);
        tvCityName.setText(result.get_city().get_cityname());
    }

    @Override
    public void onFailure(Call<SearchResult> call, Throwable t) {
        Log.e("Fail", t.getMessage());
    }
}
