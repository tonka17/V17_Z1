package hr.ferit.apoljak.v17_z1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonija on 05/10/2017.
 */

class Clouds {
    @SerializedName("all") private int _All;

    public Clouds() {
    }

    public Clouds(int _All) {
        this._All = _All;
    }

    public int getAll() {
        return _All;
    }

}
