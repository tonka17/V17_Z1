package hr.ferit.apoljak.v17_z1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonija on 05/10/2017.
 */

class City {
    @SerializedName("id") private int _id;
    @SerializedName("name") private String _cityname;
    @SerializedName("coord") Coordinate _coordinate;
    @SerializedName("country") private String _country;

    public City() {
    }

    public City(int _id, String _cityname, Coordinate _coordinate, String _country) {
        this._id = _id;
        this._cityname = _cityname;
        this._coordinate = _coordinate;
        this._country = _country;
    }

    public int get_id() {
        return _id;
    }

    public String get_cityname() {
        return _cityname;
    }

    public Coordinate get_coordinate() {
        return _coordinate;
    }

    public String get_country() {
        return _country;
    }
}
