package hr.ferit.apoljak.v17_z1;

import com.google.gson.annotations.SerializedName;

import java.util.List;


/**
 * Created by Student on 15.9.2017..
 */

class Forecast {
    @SerializedName("dt") private int _Dt;
    @SerializedName("main") MainTemp _main;
    @SerializedName("weather")List<WeatherDescription> _weather;
    @SerializedName("clouds") private Clouds _Clouds;
    @SerializedName("wind") private Wind _Wind;
    @SerializedName("rain") private Rain _Rain;
    @SerializedName("snow") private Snow _Snow;
    @SerializedName("sys") private Sys _Sys;
    @SerializedName("dt_txt") private String _DtText;


    public Forecast() {
    }

    public Forecast(int _Dt, MainTemp _main, List<WeatherDescription> _weather, Clouds _Clouds, Wind _Wind, Rain _Rain, Snow _Snow, Sys _Sys, String _DtText) {
        this._Dt = _Dt;
        this._main = _main;
        this._weather = _weather;
        this._Clouds = _Clouds;
        this._Wind = _Wind;
        this._Rain = _Rain;
        this._Snow = _Snow;
        this._Sys = _Sys;
        this._DtText = _DtText;
    }

    public int get_Dt() {
        return _Dt;
    }

    public MainTemp get_main() {
        return _main;
    }

    public List<WeatherDescription> get_weather() {
        return _weather;
    }

    public Clouds get_Clouds() {
        return _Clouds;
    }

    public Wind get_Wind() {
        return _Wind;
    }

    public Rain get_Rain() {
        return _Rain;
    }

    public Snow get_Snow() {
        return _Snow;
    }

    public Sys get_Sys() {
        return _Sys;
    }

    public String get_DtText() {
        return _DtText;
    }
}
