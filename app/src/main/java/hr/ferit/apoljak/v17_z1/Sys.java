package hr.ferit.apoljak.v17_z1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonija on 05/10/2017.
 */

class Sys {
    @SerializedName("pod") private String _pod;

    public Sys() {
    }

    public Sys(String _pod) {
        this._pod = _pod;
    }

    public String get_pod() {
        return _pod;
    }
}
