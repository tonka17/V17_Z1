package hr.ferit.apoljak.v17_z1;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Student on 15.9.2017..
 */

class SearchResult {

        @SerializedName("cod") double _cod;
        @SerializedName("message") double _message;
        @SerializedName("cnt") double _cnt;
        @SerializedName("list") List<Forecast> _forecasts;
        @SerializedName("city") City _city;

    public SearchResult() {
    }

    public SearchResult(double _cod, double _message, double _cnt, List<Forecast> _forecasts, City _city) {
        this._cod = _cod;
        this._message = _message;
        this._cnt = _cnt;
        this._forecasts = _forecasts;
        this._city = _city;
    }

    public double get_cod() {
        return _cod;
    }

    public double get_message() {
        return _message;
    }

    public double get_cnt() {
        return _cnt;
    }

    public List<Forecast> get_forecast() {
        return _forecasts;
    }

    public City get_city() {
        return _city;
    }
}
