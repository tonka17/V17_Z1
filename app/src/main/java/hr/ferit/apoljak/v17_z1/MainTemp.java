package hr.ferit.apoljak.v17_z1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Student on 15.9.2017..
 */

class MainTemp {
    @SerializedName("temp") double _temp;
    @SerializedName("temp_min") private double _TempMin;
    @SerializedName("temp_max") private double _TempMax;
    @SerializedName("pressure") private double _Pressure;
    @SerializedName("sea_level") private double _SeaLevel;
    @SerializedName("grnd_level") private double _GrndLevel;
    @SerializedName("humidity") private int _Humidity;
    @SerializedName("temp_kf") private double _TempKf;


    public MainTemp() {
    }

    public MainTemp(double _temp, double _TempMin, double _TempMax, double _Pressure, double _SeaLevel, double _GrndLevel, int _Humidity, long _TempKf) {
        this._temp = _temp;
        this._TempMin = _TempMin;
        this._TempMax = _TempMax;
        this._Pressure = _Pressure;
        this._SeaLevel = _SeaLevel;
        this._GrndLevel = _GrndLevel;
        this._Humidity = _Humidity;
        this._TempKf = _TempKf;
    }

    public double get_temp() {
        return _temp;
    }

    public double get_TempMin() {
        return _TempMin;
    }

    public double get_TempMax() {
        return _TempMax;
    }

    public double get_Pressure() {
        return _Pressure;
    }

    public double get_SeaLevel() {
        return _SeaLevel;
    }

    public double get_GrndLevel() {
        return _GrndLevel;
    }

    public int get_Humidity() {
        return _Humidity;
    }

    public double get_TempKf() {
        return _TempKf;
    }
}
