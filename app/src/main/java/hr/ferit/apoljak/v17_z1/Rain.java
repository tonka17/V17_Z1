package hr.ferit.apoljak.v17_z1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonija on 05/10/2017.
 */

class Rain {
    @SerializedName("3h") private double _3hRain;

    public Rain() {
    }

    public Rain(double _3hRain) {
        this._3hRain = _3hRain;
    }

    public double get3hRain() {
        return _3hRain;
    }

}
