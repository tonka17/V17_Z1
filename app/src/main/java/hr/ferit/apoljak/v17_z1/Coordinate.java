package hr.ferit.apoljak.v17_z1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonija on 05/10/2017.
 */

class Coordinate {
    @SerializedName("lat") private double _latitude;
    @SerializedName("lon") private double _longitude;

    public Coordinate() {
    }

    public Coordinate(double _latitude, double _longitude) {
        this._latitude = _latitude;
        this._longitude = _longitude;
    }

    public double get_latitude() {
        return _latitude;
    }

    public double get_longitude() {
        return _longitude;
    }
}
