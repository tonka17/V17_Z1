package hr.ferit.apoljak.v17_z1;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonija on 05/10/2017.
 */

class Wind {
    @SerializedName("speed") private double _speed;
    @SerializedName("deg") private double _deg;

    public Wind() {
    }

    public Wind(double _speed, double _deg) {
        this._speed = _speed;
        this._deg = _deg;
    }

    public double get_speed() {
        return _speed;
    }

    public double get_deg() {
        return _deg;
    }
}
