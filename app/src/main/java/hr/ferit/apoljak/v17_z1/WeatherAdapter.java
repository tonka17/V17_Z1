package hr.ferit.apoljak.v17_z1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Student on 15.9.2017..
 */

public class WeatherAdapter extends BaseAdapter {

    SearchResult _Weather;

    public WeatherAdapter(SearchResult _Weather) {
        this._Weather = _Weather;
    }

    @Override
    public int getCount()
    {
        return this._Weather.get_forecast().size();
    }

    @Override
    public Object getItem(int position)
    {
        return this._Weather.get_forecast().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        WeatherHolder holder;
        if(convertView==null)
        {
            convertView= LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_forecast, parent, false);
            holder = new WeatherHolder(convertView);
            convertView.setTag(holder);
        }
        else
        {
            holder = (WeatherHolder) convertView.getTag();
        }

        Forecast result = this._Weather.get_forecast().get(position);

        holder.tvDate.setText(result.get_DtText());
        holder.tvTemperature.setText("Temp " + String.valueOf(result.get_main().get_temp()));
        holder.tvTemperatureMin.setText("Min Temp " + String.valueOf(result.get_main().get_TempMin()));
        holder.tvTemperatureMax.setText("Max Temp " + String.valueOf(result.get_main().get_TempMax()));

        for (WeatherDescription WeatherDescription : result.get_weather()) {
            holder.tvWeatherMain.setText(WeatherDescription.get_Main());
            holder.tvDescription.setText(WeatherDescription.get_Description());
            Picasso.with(parent.getContext()).load(OWM_API.IMAGE_URL + WeatherDescription.get_Icon() + ".png").fit().centerCrop().into(holder.ivWeatherIcon);
        }


        return convertView;
    }

    static class WeatherHolder
    {
        @BindView(R.id.tvDate) TextView tvDate;
        @BindView(R.id.tvWeatherMain) TextView tvWeatherMain;
        @BindView(R.id.tvTemperature) TextView tvTemperature;
        @BindView(R.id.tvTemperatureMin) TextView tvTemperatureMin;
        @BindView(R.id.tvTemperatureMax) TextView tvTemperatureMax;
        @BindView(R.id.tvDescription) TextView tvDescription;
        @BindView(R.id.ivWeatherIcon) ImageView ivWeatherIcon;

        public WeatherHolder(View view)
    {
        ButterKnife.bind(this, view);
    }
    }
}
